/**
 *
 * SafeMode.js
 * Quickly engage the MediaWiki safe mode
 *
 * ----
 *
 * @author  rileyvel
 * @licence unlicense
 * @version 1
 * @since   2020-08-26
 *
 */

// wrap in big function to prevent scope leak
(function() {

    // insert custom link
    const insertionPoint = document.querySelector("#t-specialpages");

    const newElmt = insertionPoint.cloneNode(false);
    newElmt.innerHTML = `<a style="cursor: pointer">Engage Safe Mode</a>`;
    newElmt.addEventListener("click", linkOnClick);

    insertionPoint.insertAdjacentElement("afterend", newElmt);

    // link on click handler
    function linkOnClick(event) {

        // construct new url
        const newURL = new URL(window.location);
        newURL.searchParams.append("safemode", "1");

        // redirect there
        window.location.href = newURL;

    }


})();
